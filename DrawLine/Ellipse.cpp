#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;

	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc + x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	//Draw first area of 1/4 Ellipse (dx > 0, dy < 0, -1 < y' < 0)
	int x = 0;
	int y = b;
	int a2 = a*a;
	int a4 = a2*a2;
	int b2 = b*b;
	int p = 2 * b2 + a2*(1 - 2 * b);
	int const1 = 4 * b2;
	int const2 = 6 * b2;
	int const3 = 4 * a2;

	Draw4Points(xc, yc, x, y, ren);
	
	while ((a2 + b2)*x*x <= a4)
	{
		if (p <= 0)
		{
			p += const1*x + const2;
		}
		else
		{
			p += const3*(1 - y) + const1*x + const2;
			y--;
		}
		
		x++;
		
		Draw4Points(xc, yc, x, y, ren);
	}

	//Draw second area of 1/4 Ellipse (dx < 0, dy > 0, y' < -1)
	x = a;
	y = 0;
	p = 2 * a2 + b2*(1 - 2 * a);
	const1 = 4 * a2;
	const2 = 6 * a2;
	const3 = 4 * b2;

	Draw4Points(xc, yc, x, y, ren);

	while ((a2 + b2)*x*x >= a4)
	{
		if (p <= 0)
		{
			p += const1*y + const2;
		}
		else
		{
			p += const3*(1 - x) + const1*y + const2;
			x--;
		}

		y++;

		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	//Draw first area of 1/4 Ellipse (dx > 0, dy < 0, -1 < y' < 0)
	int x = 0;
	int y = b;
	int a2 = a*a, a4 = a2*a2;
	int b2 = b*b;
	int p = b2 + a2*(0.25 - b);

	Draw4Points(xc, yc, x, y, ren);

	while ((a2 + b2)*x*x <= a4)
	{
		if (p <= 0)
		{
			p += b2*(2 * x + 3);
		}
		else
		{
			p += b2*(2 * x + 3) + 2 * a2*(1 - y);
			y--;
		}
		x++;

		Draw4Points(xc, yc, x, y, ren);
	}

	//Draw second area of 1/4 Ellipse (dx < 0, dy > 0, y' < -1)
	x = a;
	y = 0;
	p = a2 + b2*(0.25 - a);

	Draw4Points(xc, yc, x, y, ren);

	while ((a2 + b2)*x*x >= a4)
	{
		if (p <= 0)
		{
			p += a2*(2 * y + 3);
		}
		else
		{
			p += a2*(2 * y + 3) + 2 * b2*(1 - x);
			x--;
		}
		y++;

		Draw4Points(xc, yc, x, y, ren);
	}
}